# README

    This file is a 'Quick Start Guide'. You will find instrucctions on how to import the project in
    your IDE and how to build it.

## Versions

	This project uses android sdk 23 to build the application. You must have sdk 23 installed on
	your computer to build. The minimum targeted version is sdk 16.
	To build the project, you also must have build tools V.23.0.2 installed.

	To get Android SDK and Android Build Tools, the easiest way is to use Android Studio, download
	 the last stable version and use the included SDK Manager.

## Application icon and name

### Application icon
    
    To change the application icon, simply replace files named ic_launcher in mipmap folder of the resources.

### Application name
    
    To change the application name, in strings.xml, change the value of string with key "app_name".

## Installed frameworks and libraries

### Android annotations

	Library's documentation is available online at  [Android annotations official site](http://androidannotations.org).

### Gson

	Gson is a reliable and effiecent JSON parser. [The GitHub project](https://github.com/google/gson)

## Project architecture

	Description of the packages :

	* Application: should only contain CustomApplication class which is intented to store static context values and make it accessible from everywhere.
	* Model: this package should contains classes representing your data
	* Activities: this package should contains your activities
	* Adapters: Classes used in Android to put data into views
	* Utils: should contains utilities that can be used anywhere in the project
	* api: This package contains classes and methods to place request on a RESTOAuthv2 API
	* views: Customized views.

## Service locator
	
	A Service locator is implemented, it allows to instanciate a service once and then accessing it from anywhere in the application.
	A service must have an interface. The implementation has to be instanciated and registered in the service locator. To retrieve a service, 
	call getService passing it the the interafce describinig it.

# Importing the project into the IDE

If you can read this file you have already unzipped the project folder.
In Android Studio, under File menu, choose Open and select the folder created when unzipping.

The project will be loaded into the IDE.

In order to import libraries, you have to sync the project with Gradle.
As Android Annotations is used in this project, before running it, you have to Build it.
Go in the Build menu and click Make Project.

You are all set, you can run the project.