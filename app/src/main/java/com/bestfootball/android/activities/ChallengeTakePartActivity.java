package com.bestfootball.android.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bestfootball.android.R;
import com.bestfootball.android.api.BestFootBallApiRequestHandler;
import com.bestfootball.android.api.UploadVideoCallback;
import com.bestfootball.android.api.UserInfoCallback;
import com.bestfootball.android.model.Participation;
import com.bestfootball.android.views.BestFootballSnackbar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Arthur Guillaume on 25/01/16.
 */
@EActivity(R.layout.challenge_take_part_form_activity)
public class ChallengeTakePartActivity extends Activity implements TextView.OnEditorActionListener {

    @Extra
    protected Participation participation;

    @ViewById(R.id.root_layout)
    protected RelativeLayout rootLayout;
    @ViewById(R.id.header_title)
    protected TextView titleView;
    @ViewById(R.id.titleField)
    protected EditText titleField;
    @ViewById(R.id.repeatsField)
    protected EditText repeatsField;
    @ViewById(R.id.submit_button)
    protected Button submitButton;
    @ViewById(R.id.progress_bar)
    protected ProgressBar progressBar;
    private Snackbar progressSnackbar;

    @AfterViews
    public void init() {
        progressSnackbar = BestFootballSnackbar.make(this, rootLayout, "Upload in progress, please don't close the app ...", Snackbar.LENGTH_INDEFINITE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            titleField.setBackgroundResource(R.drawable.bfprelollipoptheme_textfield_activated_holo_dark);
            repeatsField.setBackgroundResource(R.drawable.bfprelollipoptheme_textfield_activated_holo_dark);
        }

        if (participation != null) {
            rootLayout.requestFocus();
            repeatsField.setOnEditorActionListener(this);
            titleView.setText(participation.getChallenge().getTitle());
        } else {
            throw new IllegalStateException("ChallengeTakePartActivity was initilaized without a participation");
        }
    }

    @Click(R.id.submit_button)
    public void submit() {
        if (titleField.getText().length() == 0 || repeatsField.getText().length() == 0) {
            BestFootballSnackbar.make(this, rootLayout, "All the fields must be filled.", Snackbar.LENGTH_LONG).show();
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        submitButton.setVisibility(View.GONE);
        progressSnackbar.show();
        BestFootBallApiRequestHandler.getInstance(this).getUserInfo(new UserInfoCallback() {
            @Override
            public void onSuccess(int userId, String username) {
                String title = titleField.getText().toString();
                String repeats = repeatsField.getText().toString();
                int challengeId = participation.getChallenge().getId();
                try {
                    Integer.parseInt(repeats);
                } catch (NumberFormatException e) {
                    new Handler(getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            BestFootballSnackbar.make(ChallengeTakePartActivity.this, rootLayout, "Repeats must be a number.", Snackbar.LENGTH_LONG).show();
                            progressSnackbar.dismiss();
                            progressBar.setVisibility(View.GONE);
                            submitButton.setVisibility(View.VISIBLE);
                        }
                    });
                    return;
                }
                File video = new File(Uri.parse(participation.getUri()).getEncodedPath());
                if (!video.exists()) {
                    new Handler(getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            BestFootballSnackbar.make(ChallengeTakePartActivity.this, rootLayout, "Unable to load the video.", Snackbar.LENGTH_LONG).show();
                            progressSnackbar.dismiss();
                            progressBar.setVisibility(View.GONE);
                            submitButton.setVisibility(View.VISIBLE);
                        }
                    });
                    return;
                }
                String pathToJson = video.getAbsolutePath() + ".json";
                File data = new File(pathToJson);
                try {
                    data.createNewFile();
                    FileWriter writer = new FileWriter(data);
                    writer.write("{");
                    writer.write("\"idUser\":\"" + userId + "\",");
                    writer.write("\"idChallenge\":\"" + challengeId + "\",");
                    writer.write("\"title\":\"" + title + "\",");
                    writer.write("\"repetitions\":\"" + repeats + "\"");
                    writer.write("}");
                    writer.flush();
                    writer.close();
                } catch (IOException e) {
                    new Handler(getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            BestFootballSnackbar.make(ChallengeTakePartActivity.this, rootLayout, "Unable to save data for this participation.", Snackbar.LENGTH_LONG).show();
                            progressSnackbar.dismiss();
                            progressBar.setVisibility(View.GONE);
                            submitButton.setVisibility(View.VISIBLE);
                        }
                    });
                    return;
                }
                BestFootBallApiRequestHandler.getInstance(ChallengeTakePartActivity.this).uploadVideo(data, video, new UploadVideoCallback() {
                    @Override
                    public void onSuccess() {
                        new Handler(getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                progressSnackbar.dismiss();
                                progressBar.setVisibility(View.GONE);
                                submitButton.setVisibility(View.VISIBLE);
                            }
                        });
                        final Intent intent = new Intent(ChallengeTakePartActivity.this, ChallengesListActivity_.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        Snackbar snackbar = BestFootballSnackbar.make(ChallengeTakePartActivity.this, rootLayout, "Participation saved.", Snackbar.LENGTH_LONG);
                                snackbar.setCallback(new Snackbar.Callback() {
                                    @Override
                                    public void onDismissed(Snackbar snackbar, int event) {
                                        super.onDismissed(snackbar, event);
                                        ChallengeTakePartActivity.this.startActivity(intent);
                                    }
                                });
                        snackbar.show();
                    }

                    @Override
                    public void onError(int statusCode, String error) {
                        new Handler(getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                BestFootballSnackbar.make(ChallengeTakePartActivity.this, rootLayout, "The upload failed.", Snackbar.LENGTH_LONG).show();
                                progressSnackbar.dismiss();
                                progressBar.setVisibility(View.GONE);
                                submitButton.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                });
            }

            @Override
            public void onError(int statusCode, String error) {
                new Handler(getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        BestFootballSnackbar.make(ChallengeTakePartActivity.this, rootLayout, "Unable to determine current user.", Snackbar.LENGTH_LONG).show();
                        progressSnackbar.dismiss();
                        progressBar.setVisibility(View.GONE);
                        submitButton.setVisibility(View.VISIBLE);
                    }
                });
            }
        });
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (v == repeatsField && actionId == EditorInfo.IME_ACTION_DONE) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            submit();
            return true;
        } else {
            return false;
        }
    }
}
