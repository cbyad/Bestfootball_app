package com.bestfootball.android.activities;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bestfootball.android.R;
import com.bestfootball.android.api.BestFootBallApiRequestHandler;
import com.bestfootball.android.api.LoginCallback;
import com.bestfootball.android.api.RefreshCallback;
import com.bestfootball.android.views.BestFootballSnackbar;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.auth.api.Auth;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.Arrays;

@EActivity(R.layout.activity_login)
public class LoginActivity extends Activity implements TextView.OnEditorActionListener {

    private static final int RC_GOOGLE_SIGN_IN = 123456;

    @ViewById(R.id.layout_root)
    protected RelativeLayout layoutRoot;

    @ViewById(R.id.content_layout)
    protected ViewGroup contentLayout;

    @ViewById(R.id.username_field)
    protected EditText usernameField;

    @ViewById(R.id.password_field)
    protected EditText passwordField;

    @ViewById(R.id.facebook_login_button)
    protected LoginButton facebookLoginButton;

    @ViewById(R.id.google_login_button)
    protected SignInButton googleLoginButton;

    @ViewById(R.id.submit_button)
    protected Button submitButton;

    /*---add suscribe button by @cb_mac--- */
    @ViewById(R.id.suscribe_button)
    protected Button suscribeButton;
    /*-------------------------------*/

    @ViewById(R.id.progress_bar)
    protected ProgressBar progressBar;

    @ViewById(R.id.divider)
    protected View divider;

    private CallbackManager callbackManager;
    private GoogleApiClient googleApiClient;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGoogleSignInResult(result);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void handleGoogleSignInResult (GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            BestFootBallApiRequestHandler api = BestFootBallApiRequestHandler.getInstance(this);
            api.loginWithProvider(BestFootBallApiRequestHandler.GOOGLE_PROVIDER, account.getId(), new LoginCallback() {
                @Override
                public void onSuccess(int statusCode, String response) {
                    startActivity(new Intent(LoginActivity.this, ChallengesListActivity_.class));
                }

                @Override
                public void onError(int statusCode, String error) {
                    BestFootballSnackbar.make(LoginActivity.this, layoutRoot, "Unable to connect with Google.", Snackbar.LENGTH_LONG).show();
                }
            });
        } else {
            BestFootballSnackbar.make(this, layoutRoot, "Log in with Google failed.", Snackbar.LENGTH_LONG).show();
        }
    }

    @Click(R.id.google_login_button)
    public void loginGoogle () {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent, RC_GOOGLE_SIGN_IN);
    }

    @Click(R.id.facebook_login_button)
    public void loginFacebook () {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
    }

    @AfterViews
    public void init() {
        // Change background color if android version is below 5
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            usernameField.setBackgroundResource(R.drawable.bfprelollipoptheme_textfield_activated_holo_dark);
            passwordField.setBackgroundResource(R.drawable.bfprelollipoptheme_textfield_activated_holo_dark);
        }

        contentLayout.requestFocus();
        passwordField.setOnEditorActionListener(this);
        AnimatorSet animator = getAnimation();
        animator.setStartDelay(2000);
        animator.start();

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestId()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();

        callbackManager = CallbackManager.Factory.create();
        FacebookCallback facebookCallback = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //submitButton.setVisibility(View.GONE);
                        progressBar.setVisibility(View.VISIBLE);
                    }
                });
                final BestFootBallApiRequestHandler api = BestFootBallApiRequestHandler.getInstance(LoginActivity.this);
                GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject user, GraphResponse response) {
                        final String facebookId = user.optString("id");
                        api.loginWithProvider(BestFootBallApiRequestHandler.FACEBOOK_PROVIDER, facebookId, new LoginCallback() {
                            @Override
                            public void onSuccess(int statusCode, String response) {
                                progressBar.setVisibility(View.INVISIBLE);
                                startActivity(new Intent(LoginActivity.this, ChallengesListActivity_.class));
                            }

                            @Override
                            public void onError(int statusCode, String error) {
                                handleErrorMessage(statusCode, error);
                            }
                        });
                    }
                }).executeAsync();
            }

            @Override
            public void onCancel() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        submitButton.setVisibility(View.VISIBLE);
                        suscribeButton.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }

            @Override
            public void onError(FacebookException error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        submitButton.setVisibility(View.VISIBLE);
                        suscribeButton.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        };
        LoginManager.getInstance().registerCallback(callbackManager, facebookCallback);
    }

    @Click(R.id.submit_button)
    public void submit() {
        // I write here some things just to test PLayerConnectedMainActivity
        //TODO
        if(usernameField.getText().toString().equals("yo") && passwordField.getText().toString().equals("yo")){
            Intent intent = new Intent(LoginActivity.this,PlayerConnectedMainActivity.class);
            startActivity(intent);

        }



        if (usernameField.getText().length() == 0 || passwordField.getText().length() == 0) {
            BestFootballSnackbar.make(LoginActivity.this, layoutRoot,R.string.login_message, Snackbar.LENGTH_LONG).show();
            return;
        }
        submitButton.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        final BestFootBallApiRequestHandler api = BestFootBallApiRequestHandler.getInstance(this);
        api.login(usernameField.getText().toString(), passwordField.getText().toString(), new LoginCallback() {
            @Override
            public void onSuccess(int statusCode, String response) {
                progressBar.setVisibility(View.INVISIBLE);
                startActivity(new Intent(LoginActivity.this, ChallengesListActivity_.class));
            }

            @Override
            public void onError(int statusCode, String error) {
                handleErrorMessage(statusCode, error);
            }
        });
    }

    /*-------- suscribe button submit --------*/
    @Click(R.id.suscribe_button)
    public void suscribe() {
        //TODO
        Intent intent =new Intent(LoginActivity.this,PlayerManagerActivity_.class);
        startActivity(intent);

    }

    public void handleErrorMessage(int statusCode, String error) {
        String msg;
        if (statusCode == 400) {
            msg = "The couple username / password doesn't exist.";
        } else {
            msg = "An error occured, unable to log in.";
        }
        BestFootballSnackbar.make(this, layoutRoot, msg, Snackbar.LENGTH_LONG).show();
        progressBar.setVisibility(View.GONE);
        submitButton.setVisibility(View.VISIBLE);
        suscribeButton.setVisibility(View.VISIBLE);
    }

    private AnimatorSet getAnimation() {
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator usernameAnimator = ObjectAnimator.ofInt(usernameField, "visibility", View.VISIBLE);
        ObjectAnimator passwordAnimator = ObjectAnimator.ofInt(passwordField, "visibility", View.VISIBLE);
        ObjectAnimator dividerAnimator = ObjectAnimator.ofInt(divider, "visibility", View.VISIBLE);
        ObjectAnimator submitButtonAnimator = ObjectAnimator.ofInt(submitButton, "visibility", View.VISIBLE);
        ObjectAnimator suscribeButtonAnimator = ObjectAnimator.ofInt(suscribeButton, "visibility", View.VISIBLE);
        ObjectAnimator facebookButtonAnimator = ObjectAnimator.ofInt(facebookLoginButton, "visibility", View.VISIBLE);
        ObjectAnimator googleButtonAnimator = ObjectAnimator.ofInt(googleLoginButton, "visibility", View.VISIBLE);
        animatorSet.playTogether(usernameAnimator, passwordAnimator, submitButtonAnimator,suscribeButtonAnimator, facebookButtonAnimator, googleButtonAnimator, dividerAnimator);
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                final AnimatorSet opacityAnimatorSet = new AnimatorSet();
                ObjectAnimator opacityUsernameAnimator = ObjectAnimator.ofFloat(usernameField, "alpha", 1f);
                ObjectAnimator opacityPasswordAnimator = ObjectAnimator.ofFloat(passwordField, "alpha", 1f);
                ObjectAnimator opacitySubmitButtonAnimator = ObjectAnimator.ofFloat(submitButton, "alpha", 1f);
                ObjectAnimator opacitySuscribeButtonAnimator = ObjectAnimator.ofFloat(suscribeButton, "alpha", 1f);
                ObjectAnimator opacityFacebookButtonAnimator = ObjectAnimator.ofFloat(facebookLoginButton, "alpha", 1f);
                ObjectAnimator opacityGoogleButtonAnimator = ObjectAnimator.ofFloat(googleLoginButton, "alpha", 1f);
                ObjectAnimator opacitydividerAnimator = ObjectAnimator.ofFloat(divider, "alpha", 1f);
                opacityAnimatorSet.playTogether(opacityUsernameAnimator, opacityPasswordAnimator, opacitySubmitButtonAnimator,opacitySuscribeButtonAnimator, opacityFacebookButtonAnimator, opacityGoogleButtonAnimator, opacitydividerAnimator);

                BestFootBallApiRequestHandler api = BestFootBallApiRequestHandler.getInstance(LoginActivity.this);

                if (!api.canTryToRefresh()) {
                    opacityAnimatorSet.start();
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    api.refreshToken(new RefreshCallback() {
                        @Override
                        public void onSuccess() {
                            startActivity(new Intent(LoginActivity.this, ChallengesListActivity_.class));
                        }

                        @Override
                        public void onError(int statusCode, String error) {
                            progressBar.setVisibility(View.GONE);
                            opacityAnimatorSet.start();
                        }
                    });
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        return animatorSet;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if ((v == passwordField) && (actionId == EditorInfo.IME_ACTION_DONE)) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            submit();
            return true;
        }
        return false;
    }
}
