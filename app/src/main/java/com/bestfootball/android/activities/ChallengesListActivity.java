package com.bestfootball.android.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bestfootball.android.R;
import com.bestfootball.android.adapters.ChallengesAdapter;
import com.bestfootball.android.api.BestFootBallApiRequestHandler;
import com.bestfootball.android.api.ChallengesListCallback;
import com.bestfootball.android.model.Challenge;
import com.bestfootball.android.model.Participation;
import com.bestfootball.android.views.BestFootballSnackbar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.util.List;

/**
 * Created by Arthur Guillaume on 25/01/16.
 */

@EActivity(R.layout.challenge_list_activity)
public class ChallengesListActivity extends Activity {

    private static final int PERMISSION_REQUEST_CODE = 100;
    private static final int VIDEO_CAPTURE_REQUEST_CODE = 200;
    @ViewById(R.id.root_layout)
    protected RelativeLayout rootLayout;
    @ViewById(R.id.challenges_list_view)
    protected ListView listView;
    @ViewById(R.id.progress_bar)
    protected ProgressBar progressBar;
    private Snackbar backSnackbar;
    private Participation participation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        if (backSnackbar.isShown()) {
            BestFootBallApiRequestHandler.getInstance(this).logout();
            super.onBackPressed();
        } else {
            backSnackbar.show();
        }
    }

    @AfterViews
    public void init() {
        backSnackbar = BestFootballSnackbar.make(this, rootLayout, "Press back twice to logout", Snackbar.LENGTH_LONG);
        initChallengesList();
    }

    private void initChallengesList() {
        BestFootBallApiRequestHandler api = BestFootBallApiRequestHandler.getInstance(this);
        api.challengesList(new ChallengesListCallback() {
            @Override
            public void onSuccess(final List<Challenge> challenges) {
                progressBar.setVisibility(View.GONE);
                listView.setAdapter(new ChallengesAdapter(ChallengesListActivity.this, R.layout.challenge_block, R.id.challenge_block_title, challenges));
            }

            @Override
            public void onError(int statusCode, String error) {
                progressBar.setVisibility(View.GONE);
                BestFootballSnackbar.make(ChallengesListActivity.this, rootLayout, "An error occured.", Snackbar.LENGTH_LONG).show();
            }
        });
    }

    public Uri prepareForVideoCapture() {
        File directory = this.getExternalCacheDir();
        if (!directory.exists() || !directory.canWrite()) {
            BestFootballSnackbar.make(this, rootLayout, "An error occured while creating the file.", Snackbar.LENGTH_LONG).show();
        }
        int num = 0;
        File file = null;
        do {
            String filename = "VID_" + (++num);
            file = new File(directory.getPath() + "/" + filename + ".mp4");
        } while (file.exists());
        return Uri.fromFile(file);
    }

    public void takePart(Challenge challenge) {
        if (participation == null) {
            Uri uri = prepareForVideoCapture();
            participation = new Participation("file://" + uri.getEncodedPath(), challenge);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permissions = new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
            int cameraPermission = checkSelfPermission(android.Manifest.permission.CAMERA);
            int externalStoragePermission = checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (cameraPermission != PackageManager.PERMISSION_GRANTED || externalStoragePermission != PackageManager.PERMISSION_GRANTED) {
                SharedPreferences sharedPreferences = this.getSharedPreferences(getString(R.string.shared_preferences_permissions), Context.MODE_PRIVATE);
                if (!sharedPreferences.contains(getString(R.string.shared_preferences_camera_permission)) || !sharedPreferences.contains(getString(R.string.shared_preferences_mic_permission))) {
                    requestPermissions(permissions, PERMISSION_REQUEST_CODE);
                } else {
                    boolean cameraDenied = shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA);
                    boolean storarageDenied = shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (!(cameraDenied || storarageDenied)) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setTitle("Error");
                        builder.setMessage("Unable to record a video due to denied permissions. To take part in a challenge you must grant permissions the app requested. " +
                                "To do so, go to the settings and allow Bestfootball to access the camera and the external storage.");
                        builder.setPositiveButton("Got it", null);
                        builder.create().show();
                    } else {
                        requestPermissions(permissions, PERMISSION_REQUEST_CODE);
                    }
                }
                return;
            }
        }
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.parse(participation.getUri()));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, VIDEO_CAPTURE_REQUEST_CODE);
        } else {
            BestFootballSnackbar.make(this, rootLayout, "The camera is unavailable.", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        SharedPreferences sharedPreferences = this.getSharedPreferences(this.getString(R.string.shared_preferences_permissions), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (!sharedPreferences.contains(getString(R.string.shared_preferences_camera_permission))) {
            editor.putBoolean(getString(R.string.shared_preferences_camera_permission), true);
        }
        if (!sharedPreferences.contains(getString(R.string.shared_preferences_mic_permission))) {
            editor.putBoolean(getString(R.string.shared_preferences_mic_permission), true);
        }
        editor.apply();
        boolean permissionDenied = false;
        if (requestCode == PERMISSION_REQUEST_CODE) {
            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    permissionDenied = true;
                }
            }
            if (!permissionDenied) {
                takePart(null);
            } else {
                BestFootballSnackbar.make(this, rootLayout, "To take part in a challenge, you must grant permissions.", Snackbar.LENGTH_LONG).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VIDEO_CAPTURE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Intent intent = new Intent(this, ChallengeTakePartActivity_.class);
                if (data != null && participation != null) {
                    Uri uri = data.getData();
                    Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                    if (cursor != null) {
                        cursor.moveToFirst();
                        int idx = cursor.getColumnIndex(MediaStore.Video.VideoColumns.DATA);
                        String path = cursor.getString(idx);
                        cursor.close();
                        participation.setUri(path);
                    }
                }
                Participation mParticipation = participation;
                participation = null;
                intent.putExtra("participation", mParticipation);
                startActivity(intent);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}