package com.bestfootball.android.activities;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.view.Menu;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.bestfootball.android.R;

import java.io.FileInputStream;

public class VideoViewActivity extends Activity {

	private ProgressDialog pDialog;
	private VideoView videoview;

	//String videoURL = "http://www.androidbegin.com/tutorial/AndroidCommercial.3gp";

	String videoURL = "http://din-francis.com/video/Head-Juggles.mp4";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_video_view);
		videoview = (VideoView) findViewById(R.id.VideoView);
		// Execute StreamVideo AsyncTask
		
		// Create a progressbar
		pDialog = new ProgressDialog(VideoViewActivity.this);
		
		// Set progressbar title
		pDialog.setTitle(getString(R.string.app_name));
		
		// Set progressbar message
		pDialog.setMessage("Please wait...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(true);
		pDialog.setIcon(R.drawable.header_logo);
		pDialog.show();
		
		try {
			// Start the MediaController
			MediaController mediaController = new MediaController(VideoViewActivity.this);
			mediaController.setAnchorView(videoview);

			// Get the URL from String VideoURL
			Uri video = Uri.parse(videoURL);

			videoview.setMediaController(mediaController);
			videoview.setVideoURI(video);

		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Error", e.getMessage());
			Toast.makeText(VideoViewActivity.this,"connexion error", Toast.LENGTH_SHORT).show();
			e.printStackTrace();
		}
		
		videoview.requestFocus();
		videoview.setOnPreparedListener(new OnPreparedListener() {
			// Close the progress bar and play the video
			@Override
			public void onPrepared(MediaPlayer mp) {
				// TODO Auto-generated method stub
				pDialog.dismiss();
				videoview.start();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.video_view, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}
}
