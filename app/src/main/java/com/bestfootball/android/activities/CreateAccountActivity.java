package com.bestfootball.android.activities;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bestfootball.android.R;
import com.bestfootball.android.api.BestFootBallApiRequestHandler;
import com.bestfootball.android.api.PostRequestAPI;
import com.bestfootball.android.api.UserSignUpCallback;
import com.bestfootball.android.utils.FormChecker;
import com.bestfootball.android.views.BestFootballSnackbar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * Created by cb_mac on 27/06/17.
 */
@EActivity(R.layout.activity_create_account)
public class CreateAccountActivity extends Activity implements TextView.OnEditorActionListener{

    @ViewById(R.id.layout_root_new_account)
    protected RelativeLayout layoutRoot;

    @ViewById(R.id.content_layout_new_account)
    protected ViewGroup contentLayout;

    @ViewById(R.id.username_field_new)
    protected EditText usernameField;

    @ViewById(R.id.email_field_new)
    protected EditText emailField;

    @ViewById(R.id.password_field_new)
    protected EditText passwordField;

    @ViewById(R.id.password_field_confirm)
    protected EditText passwordFieldConfirm;


    @ViewById(R.id.register_button)
    protected Button registerButton;


    @ViewById(R.id.progress_bar_new)
    protected ProgressBar progressBar;

    @ViewById(R.id.divider_new)
    protected View divider;

    protected String email ;
    protected String username ;
    protected String password;

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return false;
    }

    @AfterViews
    public void init(){
        contentLayout.requestFocus();
        passwordField.setOnEditorActionListener(this);
        AnimatorSet animator = getAnimation();
        animator.setStartDelay(1000);
        animator.start();

    }


    @Click(R.id.register_button)
    public void register(){
        int LIMIT =3;

        if(usernameField.getText().length()==0 || emailField.getText().length()==0 ||
                passwordField.getText().length()==0 || passwordFieldConfirm.getText().length()==0){

            BestFootballSnackbar.make(CreateAccountActivity.this,layoutRoot,R.string.register_message, Snackbar.LENGTH_SHORT)
                    .show();
            return ;
        }

        else {

            if(!FormChecker.emailValidator(emailField.getText().toString())){
                BestFootballSnackbar.make(CreateAccountActivity.this,layoutRoot,R.string.email_message, Snackbar.LENGTH_SHORT)
                        .show();
                return;
            }

            if(passwordField.getText().length()<=LIMIT || passwordFieldConfirm.getText().length()<=LIMIT){
               BestFootballSnackbar.make(CreateAccountActivity.this,layoutRoot,R.string.password_protocol,Snackbar.LENGTH_SHORT)
                       .show();
                return ;
            }

            if(!passwordField.getText().toString().equals(passwordFieldConfirm.getText().toString())){
                BestFootballSnackbar.make(CreateAccountActivity.this,layoutRoot,R.string.password_message, Snackbar.LENGTH_SHORT)
                        .show();
                return ;
            }
            // Handle info and send to API

            username = usernameField.getText().toString();
            email =    emailField.getText().toString();
            password = passwordField.getText().toString();

            PostRequestAPI api = new PostRequestAPI();
            api.execute(username,email,password,"UTF-8");
            progressBar.setVisibility(View.VISIBLE);

            try {
                // Get back value from task job and handle it
                Integer result = api.get();
                handleMessage(result.intValue());

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }
    }


    private void handleMessage(int result) {
        if (result== HttpURLConnection.HTTP_CREATED){
            Intent intent =new Intent(CreateAccountActivity.this,PlayerConnectedMainActivity.class);
            intent.putExtra("first_connection",true);
            intent.putExtra("username_send",username);
            startActivity(intent);
        }

        if(result==HttpURLConnection.HTTP_CONFLICT){
            BestFootballSnackbar.make(CreateAccountActivity.this,layoutRoot,
                    "The email/username is already used",Snackbar.LENGTH_SHORT)
                    .show();
        }
           progressBar.setVisibility(View.INVISIBLE);


    }


    private AnimatorSet getAnimation() {
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator usernameAnimator = ObjectAnimator.ofInt(usernameField, "visibility", View.VISIBLE);
        ObjectAnimator emailAnimator = ObjectAnimator.ofInt(emailField, "visibility", View.VISIBLE);
        ObjectAnimator passwordAnimator = ObjectAnimator.ofInt(passwordField, "visibility", View.VISIBLE);
        ObjectAnimator confirmPasswordAnimator = ObjectAnimator.ofInt(passwordFieldConfirm, "visibility", View.VISIBLE);
        ObjectAnimator dividerAnimator = ObjectAnimator.ofInt(divider, "visibility", View.VISIBLE);
        ObjectAnimator registerButtonAnimator = ObjectAnimator.ofInt(registerButton, "visibility", View.VISIBLE);

        animatorSet.playTogether(usernameAnimator, passwordAnimator, emailAnimator,
                confirmPasswordAnimator, registerButtonAnimator, dividerAnimator);
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                final AnimatorSet opacityAnimatorSet = new AnimatorSet();
                ObjectAnimator opacityUsernameAnimator = ObjectAnimator.ofFloat(usernameField, "alpha", 1f);
                ObjectAnimator opacityPasswordAnimator = ObjectAnimator.ofFloat(passwordField, "alpha", 1f);
                ObjectAnimator opacityEmailAnimator = ObjectAnimator.ofFloat(emailField, "alpha", 1f);
                ObjectAnimator opacityPasswordConfirmAnimator = ObjectAnimator.ofFloat(passwordFieldConfirm, "alpha", 1f);
                ObjectAnimator opacityRegisterButtonAnimator = ObjectAnimator.ofFloat(registerButton, "alpha", 1f);
                ObjectAnimator opacityDividerAnimator = ObjectAnimator.ofFloat(divider, "alpha", 1f);
                opacityAnimatorSet.playTogether(opacityUsernameAnimator, opacityPasswordAnimator, opacityEmailAnimator,
                        opacityPasswordConfirmAnimator, opacityRegisterButtonAnimator, opacityDividerAnimator);

              opacityAnimatorSet.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        return animatorSet;
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();

    }

}
