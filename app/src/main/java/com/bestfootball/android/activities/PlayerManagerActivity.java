package com.bestfootball.android.activities;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bestfootball.android.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by cb_mac on 27/06/17.
 */

@EActivity(R.layout.activity_player_manager)
public class PlayerManagerActivity extends Activity {

    @ViewById(R.id.root_layout_mp)
    protected RelativeLayout rootLayout;

    @ViewById(R.id.content_layout_mp)
    protected ViewGroup contentLayout;

    @ViewById(R.id.choice_field)
    protected TextView textView;

    @ViewById(R.id.or)
    protected TextView or;

    @ViewById(R.id.manager_button)
    protected Button managerButton;

    @ViewById(R.id.player_button)
    protected Button playerButton;

    @ViewById(R.id.progress_bar_mp)
    protected ProgressBar progressBar;

    @ViewById(R.id.divider_mp)
    protected View divider;


    @AfterViews
    public void init() {
        contentLayout.requestFocus();
        AnimatorSet animator = getAnimation();
        animator.setStartDelay(1000);
        animator.start();
    }


    private AnimatorSet getAnimation() {
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator choiceAnimator = ObjectAnimator.ofInt(textView, "visibility", View.VISIBLE);
        ObjectAnimator orAnimator = ObjectAnimator.ofInt(or, "visibility", View.VISIBLE);


        ObjectAnimator dividerAnimator = ObjectAnimator.ofInt(divider, "visibility", View.VISIBLE);
        ObjectAnimator managerButtonAnimator = ObjectAnimator.ofInt(managerButton, "visibility", View.VISIBLE);
        ObjectAnimator playerButtonAnimator = ObjectAnimator.ofInt(playerButton, "visibility", View.VISIBLE);
        animatorSet.playTogether(choiceAnimator, managerButtonAnimator, playerButtonAnimator, orAnimator, dividerAnimator);
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                final AnimatorSet opacityAnimatorSet = new AnimatorSet();
                ObjectAnimator opacityChoiceAnimator = ObjectAnimator.ofFloat(textView, "alpha", 1f);
                ObjectAnimator opacityOrAnimator = ObjectAnimator.ofFloat(or, "alpha", 1f);
                ObjectAnimator opacityManagerButtonAnimator = ObjectAnimator.ofFloat(managerButton, "alpha", 1f);
                ObjectAnimator opacityPlayerButtonAnimator = ObjectAnimator.ofFloat(playerButton, "alpha", 1f);
                ObjectAnimator opacitydividerAnimator = ObjectAnimator.ofFloat(divider, "alpha", 1f);
                opacityAnimatorSet.playTogether(opacityChoiceAnimator, opacityManagerButtonAnimator,
                        opacityPlayerButtonAnimator, opacityOrAnimator, opacitydividerAnimator);

                opacityAnimatorSet.start();

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        return animatorSet;
    }

    @Click(R.id.player_button)
    public void onPlayerClick() {
        startActivity(new Intent(PlayerManagerActivity.this, CreateAccountActivity_.class));
        //TODO
        System.out.println("player button clicked");
    }


    @Click(R.id.manager_button)
    public void onManagerClick() {
        //TODO
        System.out.println("Manager button clicked test!!!!!");
       // startActivity(new Intent(PlayerManagerActivity.this, Blabla_.class));
    }

}

