package com.bestfootball.android.application;

import android.app.Application;
import android.content.Context;

import com.bestfootball.android.utils.service_locator.ServiceLocator;
import com.bestfootball.android.utils.services.CustomLogger;
import com.facebook.FacebookSdk;


/**
 * Created by Arthur on 15/05/2015.
 */
public class CustomApplication extends Application {

    private static final ServiceLocator serviceLocator = new ServiceLocator();
    private static CustomApplication instance;

    public static CustomApplication getInstance() {
        return instance;
    }

    public static Context getContext() {
        return instance;
    }

    private void registerServices() {
        CustomLogger logger = CustomLogger.getCustomLogger();
        logger.setLevel(CustomLogger.NONE);
        serviceLocator.registerService(logger);
    }

    public ServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        registerServices();
        FacebookSdk.sdkInitialize(getApplicationContext());
    }

}
