package com.bestfootball.android.adapters;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.bestfootball.android.R;
import com.bestfootball.android.activities.ChallengesListActivity;
import com.bestfootball.android.model.Challenge;

import java.util.List;


public class ChallengesAdapter extends ArrayAdapter<Challenge> {

    private static final String TAG = "ChallengesAdapter";

    private ChallengesListActivity parentActivity;

    public ChallengesAdapter(ChallengesListActivity context, int resource, int textViewResourceId, List<Challenge> objects) {
        super(context, resource, textViewResourceId, objects);
        this.parentActivity = context;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        convertView = super.getView(position, convertView, parent);
        TextView title = (TextView) convertView.findViewById(R.id.challenge_block_title);
        title.setText(getItem(position).getTitle());
        TextView description = (TextView) convertView.findViewById(R.id.challenge_block_description);
        description.setText(getItem(position).getDescription());
        Log.d(TAG, "getView: "+getItem(position).getDescription());
        Button takePartButton = (Button) convertView.findViewById(R.id.take_part_button);
        takePartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentActivity.takePart(getItem(position));
            }
        });
        return convertView;
    }
}
