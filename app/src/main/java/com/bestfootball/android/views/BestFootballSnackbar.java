package com.bestfootball.android.views;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.bestfootball.android.R;

/**
 * Created by Arthur Guillaume on 12/02/16.
 * Modified by Cb Yannick on 26/06/17
 */

public class BestFootballSnackbar {

    public static Snackbar make (Context ctx, View view, String text, int duration) {
        Snackbar sb = Snackbar.make(view, text, duration);
        sb.getView().setBackgroundColor(ContextCompat.getColor(ctx, R.color.best_football_green));
        ((TextView)sb.getView().findViewById(android.support.design.R.id.snackbar_text)).setTypeface(Typeface.createFromAsset(ctx.getAssets(), "RobotoCondensed-Bold.ttf"));
        return sb;
    }

    // this overloading allow to give a xml string ressourse ---> more maintainable
    public static Snackbar make (Context ctx, View view, int text, int duration) {
        Snackbar sb = Snackbar.make(view, text, duration);
        sb.getView().setBackgroundColor(ContextCompat.getColor(ctx, R.color.best_football_green));
        ((TextView)sb.getView().findViewById(android.support.design.R.id.snackbar_text)).setTypeface(Typeface.createFromAsset(ctx.getAssets(), "RobotoCondensed-Bold.ttf"));
        return sb;
    }


}
