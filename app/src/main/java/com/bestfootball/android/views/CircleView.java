package com.bestfootball.android.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.bestfootball.android.R;

/**
 * Created by cb_mac on 07/07/17.
 */

/**
 * basic class view used to display
 * informations like numbers of movies,
 * ranking ,... of a player
 */
public class CircleView extends View {

    private Paint p ;
    private Canvas circle;
    private String content ;

    private void init(@Nullable AttributeSet set){
        p = new Paint();
        // smooths
        p.setAntiAlias(true);

        int myColor = getResources().getColor(R.color.best_football_green_25pc);
        p.setColor(myColor);
        p.setStyle(Paint.Style.STROKE);
        p.setStrokeWidth(9f);
    }

    public CircleView(Context context) {
        super(context);
        init(null);
    }

    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CircleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CircleView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawCircle(250,230, 200, p);
        circle=canvas;
    }

    public void setContent(String c){
        content=c;
        Paint textColor = new Paint();
        textColor.setColor(Color.WHITE);
        textColor.setTextSize(50);
        textColor.setStyle(Paint.Style.STROKE);
        textColor.setStrokeWidth(9f);

        if(circle==null)Log.i("TEXT","HAHAHAHHAAH");
//      circle.drawText(content,20,100,textColor);
    }
}