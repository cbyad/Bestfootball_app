package com.bestfootball.android.api;

/**
 * Created by Arthur Guillaume on 26/01/16.
 */
public abstract class UploadVideoCallback implements Callback {
    public abstract void onSuccess();
}
