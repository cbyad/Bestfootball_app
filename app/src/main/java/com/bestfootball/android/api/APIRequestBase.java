package com.bestfootball.android.api;

import android.support.v4.util.Pair;
import android.util.Log;

import com.bestfootball.android.R;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by cb_mac on 13/07/17.
 */
public class APIRequestBase {


    private static final String LINE_FEED = "\r\n";

   // private final String boundary;
    private HttpURLConnection httpConn;
    private String charset;
    private DataOutputStream outputStream;
    private PrintWriter writer;
    private String urlParameters="";


    /**
     * This constructor initializes a new HTTP POST request with content type
     * is set to application/form-data
     *
     * @param requestURL
     * @param charset
     * @param username
     * @param email
     * @param password
     * @throws IOException
     */
    public APIRequestBase(String requestURL, String charset,String username, String email, String password)
            throws IOException {
        this.charset = charset;

        // creates a unique boundary based on time stamp
      //  boundary = "===" + System.currentTimeMillis() + "===";

        URL url = new URL(requestURL);
        httpConn = (HttpURLConnection) url.openConnection();
        urlParameters= "username="+username+"&email="+email+"&plainPassword="+password;
        System.out.println(urlParameters);

        httpConn.setRequestMethod("POST");
        httpConn.setUseCaches(false);
        httpConn.setDoOutput(true); // indicates POST method
        httpConn.setDoInput(true);
        httpConn.setRequestProperty("HOST","staging.bestfootball.fr");
        httpConn.setRequestProperty("CONTENT-TYPE",
                "application/form-data");
        //httpConn.setRequestProperty("USER-AGENT", "CodeJava Agent");
        //httpConn.setRequestProperty("Test", "Hello Bestfootball");

        outputStream = new DataOutputStream(httpConn.getOutputStream());
        outputStream.writeBytes(LINE_FEED+urlParameters);

        outputStream.flush();
        outputStream.close();

        writer = new PrintWriter(new OutputStreamWriter(outputStream,charset),
                true);

    }

    /**
     *
     * @return a couple of status code and server response
     * @throws IOException
     * @throws UnauthorizedRequestException
     */
    public Pair<Integer,String> finish() throws IOException, UnauthorizedRequestException {

        Pair<Integer,String> pair ;

        String response ="";
        //writer.append(LINE_FEED).append(urlParameters);
        writer.append(LINE_FEED).flush();
        writer.close();

        int status = httpConn.getResponseCode();
        if (status == HttpURLConnection.HTTP_OK || status == HttpURLConnection.HTTP_CREATED) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    httpConn.getInputStream()));
            String line = null;
            response += "Status: " + status + "\n";
            while ((line = reader.readLine()) != null) {
                response += line + "\n";
            }
            reader.close();
            httpConn.disconnect();
        } else if (status == HttpURLConnection.HTTP_UNAUTHORIZED) {
            throw new UnauthorizedRequestException();
        } else {
            throw new IOException("Server returned non-OK status: " + status);
        }

        pair=new Pair<>(status,response);

        return pair;
    }





}
