package com.bestfootball.android.api;

/**
 * Created by Arthur Guillaume on 27/01/16.
 */
public abstract class RefreshCallback implements Callback {

    public abstract void onSuccess ();

}
