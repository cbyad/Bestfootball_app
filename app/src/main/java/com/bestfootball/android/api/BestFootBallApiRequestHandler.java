package com.bestfootball.android.api;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.util.Pair;
import android.util.Log;

import com.bestfootball.android.R;
import com.bestfootball.android.activities.LoginActivity_;
import com.bestfootball.android.model.Challenge;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BestFootBallApiRequestHandler {

    public static final String BEST_FOOTBALL_API_PREFERENCES_NAME = "BestFootballApiPreferences";
    public static final String SHARED_PREFERENCES_KEY = "refresh_token";
    public static final int GOOGLE_PROVIDER = 1;
    public static final int FACEBOOK_PROVIDER = 2;
    protected static final String TAG = "BFApiRequestHandler";
    protected static final int HTTP_OK = 200;
    protected static final int HTTP_UNAUTHORIZED = 401;
    protected static BestFootBallApiRequestHandler instance;
    protected Context context;
    protected String accessToken;
    protected String refreshToken;

    protected BestFootBallApiRequestHandler(Context context) {
        this.context = context;
        refreshToken = context.getSharedPreferences(BEST_FOOTBALL_API_PREFERENCES_NAME, Context.MODE_PRIVATE).getString(SHARED_PREFERENCES_KEY, null);
    }

    public static synchronized BestFootBallApiRequestHandler getInstance(Context context) {
        if (instance == null) {
            instance = new BestFootBallApiRequestHandler(context);
        }
        return instance;
    }

    public boolean canTryToRefresh() {
        return refreshToken != null;
    }

    public void refreshToken(final RefreshCallback callback) {
        if (!canTryToRefresh()) {
            callback.onError(-1, "No refresh token stored");
        } else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String urlStr = context.getString(R.string.api_scheme) + "://";
                    urlStr += context.getString(R.string.api_host) + "/";
                    urlStr += "oauth/v2/token?client_id=";
                    urlStr += context.getString(R.string.client_id);
                    urlStr += "&client_secret=";
                    urlStr += context.getString(R.string.client_secret);
                    urlStr += "&grant_type=refresh_token&refresh_token=";
                    urlStr += refreshToken;
                    try {
                        URL url = new URL(urlStr);
                        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                        httpURLConnection.setRequestMethod("GET");
                        final int responseCode = httpURLConnection.getResponseCode();
                        if (responseCode == HTTP_OK) {
                            String response = "";
                            BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                            String inputLine = null;
                            while ((inputLine = in.readLine()) != null) {
                                response += inputLine + "\n";
                            }
                            in.close();
                            handleTokens(response);
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onSuccess();
                                }
                            });
                        } else {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onError(responseCode, null);

                                }
                            });
                        }
                    } catch (final Exception e) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onError(-1, e.getMessage());
                            }
                        });
                    }
                }
            }).start();
        }
    }

    private void handleTokens(String response) {
        AccessTokenObject obj = new Gson().fromJson(response, AccessTokenObject.class);
        accessToken = obj.getAccessToken();
        refreshToken = obj.getRefreshToken();
        SharedPreferences.Editor editor = context.getSharedPreferences(BEST_FOOTBALL_API_PREFERENCES_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(SHARED_PREFERENCES_KEY, refreshToken);
        editor.apply();
    }

    public void login(final String username, final String password, final LoginCallback callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String urlStr = context.getString(R.string.api_scheme) + "://";
                urlStr += context.getString(R.string.api_host) + "/";
                urlStr += "oauth/v2/token?client_id=";
                urlStr += context.getString(R.string.client_id);
                urlStr += "&client_secret=";
                urlStr += context.getString(R.string.client_secret);
                urlStr += "&grant_type=password&username=";
                urlStr += username;
                urlStr += "&password=";
                urlStr += password;
                try {
                    URL url = new URL(urlStr);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("GET");
                    final int responseCode = httpURLConnection.getResponseCode();
                    if (responseCode == HTTP_OK) {
                        String response = "";
                        BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                        String inputLine = null;
                        while ((inputLine = in.readLine()) != null) {
                            response += inputLine + "\n";
                        }
                        in.close();
                        final String finalResponse = response;
                        handleTokens(response);
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onSuccess(responseCode, finalResponse);
                            }
                        });
                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onError(responseCode, null);

                            }
                        });
                    }
                } catch (final Exception e) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onError(-1, e.getMessage());
                        }
                    });
                }
            }
        }).start();
    }

    public void loginWithProvider(final int provider, final String externalId, final LoginCallback callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String providerName = "";
                switch (provider) {
                    case GOOGLE_PROVIDER:
                        providerName = "google";
                        break;
                    case FACEBOOK_PROVIDER:
                        providerName = "facebook";
                        break;
                }
                String urlStr = context.getString(R.string.api_scheme) + "://";
                urlStr += context.getString(R.string.api_host) + "/";
                urlStr += "oauth/v2/token?client_id=";
                urlStr += context.getString(R.string.client_id);
                urlStr += "&client_secret=";
                urlStr += context.getString(R.string.client_secret);
                urlStr += "&grant_type=http://platform.local/grants/" + providerName + "&id=";
                urlStr += externalId;
                urlStr += "&password=";
                urlStr += externalId;
                try {
                    URL url = new URL(urlStr);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("GET");
                    final int responseCode = httpURLConnection.getResponseCode();
                    if (responseCode == HTTP_OK) {
                        String response = "";
                        BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                        String inputLine = null;
                        while ((inputLine = in.readLine()) != null) {
                            response += inputLine + "\n";
                        }
                        in.close();
                        final String finalResponse = response;
                        handleTokens(response);
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onSuccess(responseCode, finalResponse);
                            }
                        });
                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onError(responseCode, null);

                            }
                        });
                    }
                } catch (final Exception e) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onError(-1, e.getMessage());
                        }
                    });
                }
            }
        }).start();
    }

    private String buildUrl(String endpoint) {
        String urlStr = context.getString(R.string.api_scheme);
        urlStr += "://" + context.getString(R.string.api_host) + "/";
        urlStr += context.getString(R.string.api_root) + "/";
        urlStr += endpoint;
        urlStr += "?access_token=" + accessToken;
        return urlStr;
    }

    public void getUserInfo(final UserInfoCallback callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(buildUrl("users/getCurrent"));
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setRequestMethod("GET");
                    final int responseCode = con.getResponseCode();
                    if (responseCode == HTTP_OK) {
                        String response = "";
                        BufferedReader is = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String line = null;
                        while ((line = is.readLine()) != null) {
                            response += line;
                        }
                        is.close();
                        JsonObject jsonObject = new JsonParser().parse(response).getAsJsonObject();
                        JsonObject userObject = jsonObject.getAsJsonObject("user");
                        final int userId = userObject.getAsJsonPrimitive("id").getAsInt();
                        final String username = userObject.getAsJsonPrimitive("username").getAsString();
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onSuccess(userId, username);
                            }
                        });
                    } else if (responseCode == HTTP_UNAUTHORIZED) {
                        if (canTryToRefresh()) {
                            refreshToken(new RefreshCallback() {
                                @Override
                                public void onSuccess() {
                                    getUserInfo(callback);
                                }

                                @Override
                                public void onError(int statusCode, String error) {
                                    logout();
                                    Intent intent = new Intent(context, LoginActivity_.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    context.startActivity(intent);
                                }
                            });
                        }
                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onError(responseCode, null);
                            }
                        });
                    }
                } catch (final IOException e) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onError(-1, e.getMessage());
                        }
                    });
                }
            }
        }).start();
    }

    public void challengesList(final ChallengesListCallback callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(buildUrl("challenges/getAll"));
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setRequestMethod("GET");
                    final int responseCode = con.getResponseCode();
                    if (responseCode == HTTP_OK) {
                        String response = "";
                        BufferedReader is = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String line = null;
                        while ((line = is.readLine()) != null) {
                            response += line;
                        }
                        is.close();
                        Log.d(TAG, "run: " + response);
                        JsonObject jsonObject = new JsonParser().parse(response).getAsJsonObject();
                        JsonArray challenges = jsonObject.get("challenges").getAsJsonArray();
                        Iterator<JsonElement> it = challenges.iterator();
                        final List<Challenge> challengeList = new ArrayList<Challenge>();
                        while (it.hasNext()) {
                            JsonObject object = it.next().getAsJsonObject();
                            challengeList.add(new Challenge(object.get("id").getAsInt(), object.get("title").getAsString(), object.get("description").getAsString()));
                        }
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onSuccess(challengeList);
                            }
                        });
                    } else if (responseCode == HTTP_UNAUTHORIZED) {
                        if (canTryToRefresh()) {
                            refreshToken(new RefreshCallback() {
                                @Override
                                public void onSuccess() {
                                    challengesList(callback);
                                }

                                @Override
                                public void onError(int statusCode, String error) {
                                    logout();
                                    Intent intent = new Intent(context, LoginActivity_.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    context.startActivity(intent);
                                }
                            });
                        }
                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onError(responseCode, null);
                            }
                        });
                    }
                } catch (final IOException e) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onError(-1, e.getMessage());
                        }
                    });
                }
            }
        }).start();

    }

    public void uploadVideo(final File data, final File video, final UploadVideoCallback callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String url = buildUrl("videos/post");
                    MultipartUtility multipart = new MultipartUtility(url, "UTF-8");

                    multipart.addFilePart("data", data);
                    multipart.addFilePart("file", video);

                    String response = multipart.finish();
                    Log.d(TAG, "run: response -> " + response);
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "run: success");
                            callback.onSuccess();
                        }
                    });

                } catch (final IOException ex) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.e(TAG, "run: ", ex);
                            callback.onError(-1, ex.getMessage());
                        }
                    });
                } catch (UnauthorizedRequestException e) {
                    if (canTryToRefresh()) {
                        refreshToken(new RefreshCallback() {
                            @Override
                            public void onSuccess() {
                                Log.d(TAG, "onSuccess: refreshed token");
                                uploadVideo(data, video, callback);
                            }

                            @Override
                            public void onError(int statusCode, String error) {
                                Log.e(TAG, "onError: unabled to refresh token");
                                logout();
                                Intent intent = new Intent(context, LoginActivity_.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                context.startActivity(intent);
                            }
                        });
                    }
                }
            }
        }).start();
    }

    public void logout() {
        refreshToken = null;
        accessToken = null;
        SharedPreferences.Editor editor = context.getSharedPreferences(BEST_FOOTBALL_API_PREFERENCES_NAME, Context.MODE_PRIVATE).edit();
        editor.remove(SHARED_PREFERENCES_KEY);
        editor.commit();
    }



    //---------------------------------------------------------------------------------------


    public void createNewUser(final String username,final String email,final  String password ,final UserSignUpCallback callback){

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    String url = buildUserUrl("api/user-register");

                    APIRequestBase  requestBase = new APIRequestBase(url,"UTF-8",username,email,password);
                    Pair<Integer,String> response = requestBase.finish();


                    Log.d(TAG, "run: response -> " + response);

                    final int statusCode = response.first;

                    System.out.println("status " + statusCode);
                    System.out.println(response.second);


                    if(statusCode== HttpURLConnection.HTTP_OK || statusCode==HttpURLConnection.HTTP_CREATED ){
                        System.out.println("WELCOME ACCOUNT WELL CREATED with status " + statusCode);
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG, "run: success");
                                callback.onSuccess(statusCode,username);
                            }
                        });


                        return;
                    }

                    else callback.onError(statusCode,response.second);

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (UnauthorizedRequestException e) {
                    callback.onError(0000,"error");
                    e.printStackTrace();
                }

                //TODO


            }
        }).start();


    }

    public String buildUserUrl(String endpoint){

        String urlStr = "http://staging.bestfootball.fr/";
        urlStr+=endpoint;
        return urlStr;
    }









}
