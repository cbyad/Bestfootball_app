package com.bestfootball.android.api;

import com.bestfootball.android.model.Challenge;

import java.util.List;


public abstract class ChallengesListCallback implements Callback {
    public abstract void onSuccess(List<Challenge> challenges);
}
