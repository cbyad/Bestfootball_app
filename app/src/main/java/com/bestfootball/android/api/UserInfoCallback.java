package com.bestfootball.android.api;


public abstract class UserInfoCallback implements Callback {
    public abstract void onSuccess(int userId, String username);
}
