package com.bestfootball.android.api;

import com.google.gson.annotations.SerializedName;

/**
 * This class is meant to manage access tokens.
 * It represents the response of the server giving a new access token
 */
public class AccessTokenObject {

    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("refresh_token")
    private String refreshToken;

    @SerializedName("expires_in")
    private int expiresIn;

    @SerializedName("token_type")
    private String tokenType;

    @SerializedName("scope")
    private String scope;

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

}
