    package com.bestfootball.android.api;

    import android.os.AsyncTask;
    import android.util.Log;


    import java.io.BufferedReader;
    import java.io.IOException;
    import java.io.InputStreamReader;
    import java.io.OutputStreamWriter;
    import java.io.UnsupportedEncodingException;
    import java.io.Writer;
    import java.net.HttpURLConnection;
    import java.net.MalformedURLException;
    import java.net.URL;
    import java.net.URLEncoder;

    /**
     * Created by cb_mac on 24/07/17.
     */
    public class PostRequestAPI  extends AsyncTask<String, Void, Integer> {

        private final  String url = "http://staging.bestfootball.fr/api/user-register";
        private int backCode; // used to know what's server code is returned
        private String response="";

        public String getResponse(){return response;};


        @Override
        protected Integer doInBackground(String... strings) {
            String username = strings[0];
            String email = strings[1];
            String password = strings[2];
            String charset = strings[3];
            BufferedReader reader = null;
            String urlParameters="";

            try {

                //prepare  parameters request
                 urlParameters = URLEncoder.encode("username", charset)
                        + "=" + URLEncoder.encode(username, charset);
                urlParameters += "&" + URLEncoder.encode("email", charset)
                        + "=" + URLEncoder.encode(email, charset);
                urlParameters += "&" + URLEncoder.encode("plainPassword", charset)
                        + "=" + URLEncoder.encode(password, charset);


                 } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                //
                try{

                URL host = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) host.openConnection();

                    conn.setDoOutput(true);
                    conn.setDoInput(true);
                    conn.setUseCaches(false);
                    conn.setRequestMethod("POST");

                    Writer writer = new OutputStreamWriter(conn.getOutputStream());
                    writer.write(urlParameters);
                    writer.flush();
                    writer.close();

                // Get the server response

                StringBuilder sb = new StringBuilder();
                String line=null ;

                int status = conn.getResponseCode();
                backCode=status;
                System.out.println("statut code ------> "+status);

                if (status == HttpURLConnection.HTTP_OK || status == HttpURLConnection.HTTP_CREATED) {
                     reader = new BufferedReader(new InputStreamReader(
                            conn.getInputStream()));

                    // Read Server Response
                    while ((line = reader.readLine()) != null) {
                        // Append server response in string
                        sb.append(line + "\n");
                    }
                    response=sb.toString();
                    reader.close();

                } else if (status==HttpURLConnection.HTTP_CONFLICT){

                }

               // System.out.println(sb.toString());


                } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                    e.printStackTrace();
                }
             finally
                {
                try {

                    reader.close();

                } catch (Exception ex) {
                }

            }

            return backCode ;
        }


        @Override
        protected void onPostExecute(Integer aVoid) {
            super.onPostExecute(aVoid);

            System.out.println("Job Finish..." + aVoid.intValue());
            if(!response.equals(""))
                System.out.println(response);
        }
    }

