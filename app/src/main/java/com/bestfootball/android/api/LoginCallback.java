package com.bestfootball.android.api;

public abstract class LoginCallback implements Callback {
    public abstract void onSuccess(int statusCode, String response);
}
