package com.bestfootball.android.api;

/**
 * Created by Arthur Guillaume on 26/01/16.
 */
interface Callback {
    void onError(int statusCode, String error);
}
