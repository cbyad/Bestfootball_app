package com.bestfootball.android.model;

import java.io.Serializable;

/**
 * Created by Arthur Guillaume on 25/01/16.
 */
public class Challenge implements Serializable {

    private int id;
    private String title;
    private String description;

    public Challenge() {
    }

    public Challenge(int id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
