package com.bestfootball.android.model;

/**
 * Created by cb_mac on 04/07/17.
 */
public enum PositionField {
    GOAL_KEEPER, DEFENSIVE, MIDFIELD, ATTACK
}
