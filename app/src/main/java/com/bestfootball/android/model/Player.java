package com.bestfootball.android.model;

/**
 * Created by cb_mac on 04/07/17.
 */
public class Player extends BestfootballUser{

    protected PositionField position ; // enum
    protected FootCategory foot ;

    protected int classement ;
    protected int grade ;

    public Player(){
        super();
    }
    public Player(PositionField position, FootCategory foot, int classement, int grade) {
        this.position = position;
        this.foot = foot;
        this.classement = classement;
        this.grade = grade;
    }

    public PositionField getPosition() {
        return position;
    }

    public void setPosition(PositionField position) {
        this.position = position;
    }

    public FootCategory getFoot() {
        return foot;
    }

    public void setFoot(FootCategory foot) {
        this.foot = foot;
    }

    public int getClassement() {
        return classement;
    }

    public void setClassement(int classement) {
        this.classement = classement;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }
}
