package com.bestfootball.android.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by cb_mac on 04/07/17.
 */
public abstract class BestfootballUser implements Serializable {

    private static final String TAG = "BESTFOOTBALLUSER" ;

    /*Perso basic*/
    protected String name ;
    protected String firstName ;
    protected String Country ;
    protected String city ;
    protected String state ;
    protected String gender ;
    protected String club;
    protected Date birthday;


    /*Mail notifications*/
    protected boolean notificationCommentVideos;
    protected boolean notificationDuel ;

    /*important Profil*/
    protected String username;
    protected String password;
    protected String profilPicture;


    public BestfootballUser(){
    }

    public BestfootballUser(String username, String password, String name, String firstName) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.firstName = firstName;
    }

    public BestfootballUser(String name, String firstName, String country, String city, String state, String gender, String club, Date birthday, boolean notificationCommentVideos, boolean notificationDuel, String username, String password, String profilPicture) {
        this.name = name;
        this.firstName = firstName;
        Country = country;
        this.city = city;
        this.state = state;
        this.gender = gender;
        this.club = club;
        this.birthday = birthday;
        this.notificationCommentVideos = notificationCommentVideos;
        this.notificationDuel = notificationDuel;
        this.username = username;
        this.password = password;
        this.profilPicture = profilPicture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public boolean isNotificationCommentVideos() {
        return notificationCommentVideos;
    }

    public void setNotificationCommentVideos(boolean notificationCommentVideos) {
        this.notificationCommentVideos = notificationCommentVideos;
    }

    public boolean isNotificationDuel() {
        return notificationDuel;
    }

    public void setNotificationDuel(boolean notificationDuel) {
        this.notificationDuel = notificationDuel;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfilPicture() {
        return profilPicture;
    }

    public void setProfilPicture(String profilPicture) {
        this.profilPicture = profilPicture;
    }


}
