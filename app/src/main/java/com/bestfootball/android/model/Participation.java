package com.bestfootball.android.model;

import java.io.Serializable;


public class Participation implements Serializable {

    private static final String TAG = "Participation";

    private String uri;
    private Challenge challenge;

    public Participation() {
    }

    public Participation(String uri, Challenge challenge) {
        this.uri = uri;
        this.challenge = challenge;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Challenge getChallenge() {
        return challenge;
    }

    public void setChallenge(Challenge challenge) {
        this.challenge = challenge;
    }
}
