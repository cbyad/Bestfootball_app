package com.bestfootball.android.utils.services;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.bestfootball.android.application.CustomApplication;


/**
 * Created by Arthur on 15/05/2015.
 */
public class ConnectionChecker {

    private static NetworkInfo getActiveConnection () {
        Context context = CustomApplication.getContext();
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public static boolean isConnected () {
        NetworkInfo activeNetwork = getActiveConnection();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static boolean isConnectedViaWifi () {
        if (isConnected()) {
            NetworkInfo activeNetwork = getActiveConnection();
            return activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
        } else {
            return false;
        }
    }

}
