package com.bestfootball.android.utils.services;

import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Arthur on 15/05/2015.
 */
public class CustomLogger implements ICustomLogger {

    private static CustomLogger customLogger;
    private static Logger logger;
    private String msgPrefix;
    private int level;

    private CustomLogger() {
        msgPrefix = "Custom logger: ";
        logger = Logger.getLogger("Custom Logger");
        logger.setLevel(Level.ALL);
        Handler handler = new ConsoleHandler();
        handler.setLevel(Level.FINE);
        logger.addHandler(handler);
        level = INFO;
    }

    public static synchronized CustomLogger getCustomLogger() {
        if (customLogger == null) {
            customLogger = new CustomLogger();
        }
        return customLogger;
    }

    public int getLevel () {
        return level;
    }

    public void setLevel (int level) {
        this.level = level;
    }

    private String buildLogMessage (String msg) {
        return msgPrefix+": "+msg;
    }

    public void error (String msg) {
        if (level <= ERROR) {
           logger.severe("Error: " + buildLogMessage(msg));
        }
    }

    public void warning (String msg) {
        if (level <= WARNING) {
            logger.warning("Warning: " + buildLogMessage(msg));
        }
    }

    public void debug (String msg) {
        if (level <= DEBUG) {
            logger.info("Debug: " + buildLogMessage(msg));
        }
    }

    public void info (String msg) {
        if (level <= INFO) {
            logger.fine("Info: " + buildLogMessage(msg));
        }
    }

}
