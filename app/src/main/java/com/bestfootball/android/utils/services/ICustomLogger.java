package com.bestfootball.android.utils.services;

/**
 * Created by Arthur on 18/05/2015.
 */
public interface ICustomLogger {

        int INFO = 0;
        int DEBUG = 1;
        int WARNING = 2;
        int ERROR = 3;
        int NONE = 4;

        int getLevel ();

        void setLevel (int level) ;

        void error (String msg);

        void warning (String msg);

        void debug (String msg);

        void info (String msg);

    }