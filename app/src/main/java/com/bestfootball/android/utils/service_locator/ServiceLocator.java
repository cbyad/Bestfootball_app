package com.bestfootball.android.utils.service_locator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arthur on 18/05/2015.
 */
public class ServiceLocator<T> {

    private List<T> services;

    public ServiceLocator () {
        services = new ArrayList<>();
    }

    public void registerService (T instance) {
        services.add(instance);
    }

    public T getService (Class<?> type) {
        for (T obj : services) {
            if (type.isAssignableFrom(obj.getClass())){
                return obj;
            }
        }
        return null;
    }

}
